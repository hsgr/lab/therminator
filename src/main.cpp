#include <Adafruit_MAX31855.h>
#include <Arduino.h>
#include <Arduino_JSON.h>
#ifdef ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <LittleFS.h>
#include <SPI.h>
#include <pinout.h>

// sensor chip select pin definitions
#ifdef ESP32
#define CS_A 22
#define CS_B 21
#define CS_C 17
#define CS_D 16
#define CS_E 27
#define CS_F 14
#define CS_G 12
#define CS_H 13
#else
#define CS_A D0
#define CS_B D1
#define CS_C D2
#define CS_D D3
#define CS_E D4
#define CS_F D7
#endif

Adafruit_MAX31855 thermocoupleA(CS_A);
Adafruit_MAX31855 thermocoupleB(CS_B);
Adafruit_MAX31855 thermocoupleC(CS_C);
Adafruit_MAX31855 thermocoupleD(CS_D);
Adafruit_MAX31855 thermocoupleE(CS_E);
Adafruit_MAX31855 thermocoupleF(CS_F);
#ifdef ESP32
Adafruit_MAX31855 thermocoupleG(CS_G);
Adafruit_MAX31855 thermocoupleH(CS_H);
#endif

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
DNSServer dns;
AsyncWiFiManager wm(&server, &dns);

JSONVar readings;

// data acquisition functions
unsigned long int lastTime = 0;
unsigned long int timerDelay = 500;
String checkSensor(Adafruit_MAX31855 &sensor) {
    /*
    return sensor value if it's numerical. if it isn't,
    check for errors and return current error.
   */
    if (isnan(sensor.readCelsius())) {
        uint8_t err = sensor.readError();
        if (err & MAX31855_FAULT_OPEN) {
            return "FAULT: thermocouple open";
        }
        if (err & MAX31855_FAULT_SHORT_GND) {
            return "FAULT: thermocouple short to GND";
        }
        if (err & MAX31855_FAULT_SHORT_VCC) {
            return "FAULT: thermocouple short to VCC";
        }
    }
    return String(sensor.readCelsius());
}
String getSensorReadings() {
    /*
    return sensor readings along with timestamp in JSON format
    */
    readings["time"] = String(lastTime);
    readings["tempA"] = checkSensor(thermocoupleA);
    readings["tempB"] = checkSensor(thermocoupleB);
    readings["tempC"] = checkSensor(thermocoupleC);
    readings["tempD"] = checkSensor(thermocoupleD);
    readings["tempE"] = checkSensor(thermocoupleE);
    readings["tempF"] = checkSensor(thermocoupleF);
#ifdef ESP32
    readings["tempG"] = checkSensor(thermocoupleG);
    readings["tempH"] = checkSensor(thermocoupleH);
#endif
    String jsonString = JSON.stringify(readings);
    return jsonString;
}
// websocket functions
void notifyClients(String text) {
    ws.textAll(text);
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
        data[len] = 0;
        String message = (char *)data;
        // check if message is "getReadings"
        if (strcmp((char *)data, "getReadings") == 0) {
            // if it is, send current readings to server
            String sensorReadings = getSensorReadings();
            Serial.printf("%s\n", sensorReadings.c_str());
            notifyClients(sensorReadings);
        }
        // check if message is an update interval change
        if (message.startsWith("u")) {
            // if it is, strip the first character, convert to int and update timerDelay variable
            String uVal = (message.substring(message.indexOf("u") + 1, message.length()));
            int uInt = uVal.toInt();
            timerDelay = uInt;
            Serial.printf("update interval changed to %d\n", uInt);
            notifyClients("update interval changed to " + uVal);
        }
    }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
    switch (type) {
        case WS_EVT_CONNECT:
            Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
            break;
        case WS_EVT_DISCONNECT:
            Serial.printf("WebSocket client #%u disconnected\n", client->id());
            break;
        case WS_EVT_DATA:
            handleWebSocketMessage(arg, data, len);
            break;
        case WS_EVT_PONG:
        case WS_EVT_ERROR:
            break;
    }
}
// initialization functions
void initSerial() {
    Serial.begin(115200);
    while (!Serial) delay(1);
    delay(5000);  // prevent garbage on serial output
    Serial.print("serial init\n");
}
void initSPI() {
#ifdef ESP32
    SPIClass hspi(HSPI);
    hspi.begin();
#else
    SPI.begin();
#endif
    Serial.printf("SPI init\n");
    Serial.printf("using pins:\nPOCI: %d\tCLK: %d\nCS_A: %d\tCS_B: %d\n", MISO, SCK, CS_A, CS_B);
}
void initLittleFS() {
    LittleFS.begin();
    Serial.printf("LittleFS init\n");
}
void initSensor(Adafruit_MAX31855 &sensor, const char *id) {
    if (!sensor.begin()) {
        Serial.printf("sensor %s init error\n", id);
        while (true) delay(10);
    }
    Serial.printf("sensor %s init\n", id);
}
void initWiFi() {
    WiFi.mode(WIFI_AP_STA);
    WiFi.setHostname("therminator");
    Serial.printf("wifi init\n");
    wm.autoConnect("therminator");
    Serial.printf("connected to %s!\nhostname: %s\nip addr: %s\n", WiFi.SSID().c_str(), String(WiFi.getHostname()).c_str(), WiFi.localIP().toString().c_str());
}

void initWebSocket() {
    ws.onEvent(onEvent);
    server.addHandler(&ws);
    Serial.printf("websocket init\n");
}
void initWebServer() {
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(LittleFS, "/index.html", "text/html");
    });
    server.on("/about", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(LittleFS, "/about.html", "text/html");
    });
    server.onNotFound([](AsyncWebServerRequest *request) {
        request->send(404, "text/plain", "404 Not Found");
    });
    server.on("/int", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/plain", String(timerDelay).c_str());
    });
#ifdef ESP32
    server.on("/isESP32", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/plain", String(true));
    });
#else
    server.on("/isESP32", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/plain", String(false));
    });
#endif
    server.serveStatic("/", LittleFS, "/");
    server.begin();
    Serial.printf("webserver init\n");
}

void setup() {
    initSerial();
    initSPI();
    initLittleFS();
    initSensor(thermocoupleA, "A");
    initSensor(thermocoupleB, "B");
    initSensor(thermocoupleC, "C");
    initSensor(thermocoupleD, "D");
    initSensor(thermocoupleE, "E");
    initSensor(thermocoupleF, "F");
#ifdef ESP32
    initSensor(thermocoupleG, "G");
    initSensor(thermocoupleH, "H");
#endif
    initWiFi();
    initWebSocket();
    initWebServer();
}

void loop() {
    if ((millis() - lastTime) > timerDelay) {
        String sensorReadings = getSensorReadings();
        Serial.println(sensorReadings);
        notifyClients(sensorReadings);
        lastTime = millis();
    }

    ws.cleanupClients();
}
