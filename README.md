# therminator
![therminator logo](therminator_logo.png)
### a modular ESP8266/ESP32 + MAX31855 based logging thermometer

therminator is a logging thermometer, based on the ESP8266/ESP32 microcontrollers and the MAX31855 thermocouple to digital converter.

### features:

- support for up to 6 (ESP8266) or 8 (ESP32) sensors 
- live temperature charts
- data recording capabilities for writing logged data to CSV files
- timed recording (recording stops after specified time passes)